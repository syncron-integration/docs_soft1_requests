
# SYNCRON x Soft1 API Requirements

Παρακάτω περιγράφουμε τα WS calls που θα χρειαστούν για την διασύνδεση Soft1 με e-shops που χρησιμοποιούν το SYNCRON (syncron.gr).

Οι κλήσεις που περιγράφονται παρακάτω αφορούν στάδια συγχρονισμού μετά την αυθεντικοποίηση, και περιέχουν clientID/token είτε στο json body, είτε σε HTTP header.

## Get products / Updated products

```http
  GET /api/items
```

Επιστρέφει όλα τα προϊόντα

```http
   GET /api/updatedItems
   ```

Επιστρέφει όλα τα προϊόντα που έχουν οποιαδήποτε ενημέρωση, από την παρεχόμενη ημερομηνία και έπειτα.

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `token` | `string` | **Required**. Authentication token |
| `packagenumber` | `integer` | Optional. The pagination offset |
| `packagesize` | `integer` | Optional. The actual size of every package (page) |
| `dateFrom` | `date` | Required on the "updatedItems" call, YYYY-MM-DD |



#### Response

```json
{
	"Success": true,
	"Data": [{
		"ITEM_ID": "e138f54d-7686-ec11-a6c1-d4ae5269035f", // μοναδικό αναγνωριστικό στο ERP
		"ITEM_CODE": "5871", // προεραιτικός κωδικός ERP
		"ITEM_SKU": "8024273905055", // SKU γονικού προΪόντος
		"ITEM_BARCODE": "KAB02", // Barcode παραλλαγής (σε μεταβλητα προϊόντα)
		"ITEM_CATEGORIES": "732EE83F-FB47-EE11-A6DB-D4AE52690360;99075646-FB47-EE11-A6DB-D4AE52690360;", // GUIDs κατηγοριών με ορισμένο separator (πχ semicolon)
		"ITEM_GR_NAME": "Ονομασία προϊόντος",
		"ITEM_ENG_NAME": null,
		"VAT_CODE": "#00001",
		"VAT_NAME": "Κανονικός ΦΠΑ",
		"WHOLESALES_PRICE": 1.0340000,
		"RETAIL_PRICE": 2.1800010,
		"WEB_PRICE": null,
		"WEB_OFFER": null,
		"B2B_PRICE": null,
		"B2C_DISCOUNT": 0.0000000,
		"BRAND_NAME": "Κατασκευαστής",
		"ACTIVE_B2C": 1,
		"ACTIVE_B2Β": 0,
		"LENGTH": 5.5000000,
		"HEIGHT": 13.5000000,
		"WIDTH": 3.0000000,
		"WEIGHT": 0.0900000,
		"VOLUME": null,
		"UPDATEDATE": "2023-09-06T15:47:30.437",
		"TYPE_GR_DESC": "RTF", // Enum. RTF ή HTML
		"GR_DESCRIPTION": "", // Ελληνική περιγραφή προϊόντος, σε base64 μορφή
		"TYPE_HTML_DESC": "RTF",// Enum. RTF ή HTML
		"ENG_DESCRIPTION": "", // Αγγλική περιγραφη προϊόντος (εάν υπάρχει), σε base64 μορφή
	}]
}
```

<br><br>

#
## Get products stock

```http
   GET /api/itemsStock
   ```

Επιστρέφει το διαθέσιμο stock για όλα τα προϊόντα, ανεξαρτήτου ημερομηνίας ενημέρωσης.

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `token` | `string` | **Required**. Authentication token |
| `packagenumber` | `integer` | Optional. The pagination offset |
| `packagesize` | `integer` | Optional. The actual size of every package (page) |



#### Response

```json
{
	"Success": true,
	"Data": [{
		"ITEMCODE": "1234", // προεραιτικός κωδικός ERP. ή ITEMID (μοναδικό αναγνωριστικό στο ERP)
		"ITEMNAME": "Ονομασία προϊόντος",
        "BARCODE": null, // το barcode της παραλλαγής σε περίπτωση μεταβλητού προϊόντος
		"QUANTITY": 13.0000000,
		"UPDATEDATE": "2022-02-05T13:31:01.503"
	}]
}
```
<br><br>

#
## Get product categories

```http
   GET /api/categories
   ```

Επιστρέφει όλες τις διαθέσιμες κατηγορίες προϊόντων.

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `token` | `string` | **Required**. Authentication token |
| `packagenumber` | `integer` | Optional. The pagination offset |
| `packagesize` | `integer` | Optional. The actual size of every package (page) |



#### Response

```json
{
	"Success": true,
	"Data": [{
		"CATEGORY_ID": "722ee83f-fb47-ee11-a6db-d4ae52690360", // μοναδικό αναγνωριστικό της κατηγορίας στο ERP
		"CATEGORY_CODE": "000001", // προαιρετικός κωδικός κατηγορίας
		"CATEGORY_NAME": "Τεστ 1", // όνομα κατηγορίας
		"PARENT_ID": null, // αναγνωριστικό γονικής κατηγορίας (εάν υπάρχει)
	}, {
		"CATEGORY_ID": "732ee83f-fb47-ee11-a6db-d4ae52690360",
		"CATEGORY_CODE": "000002",
		"CATEGORY_NAME": "Τεστ 2",
		"PARENT_ID": null,
	}, {
		"CATEGORY_ID": "99075646-fb47-ee11-a6db-d4ae52690360",
		"CATEGORY_CODE": "000003",
		"CATEGORY_NAME": "Τεστ 11",
		"PARENT_ID": "722ee83f-fb47-ee11-a6db-d4ae52690360",
	}]
}
```


<br><br>

#
## Get product photos

```http
   GET /api/photos
   ```

Επιστρέφει όλες τις διαθέσιμες εικόνες ενός ή περισσότερων προϊόντων που περιέχονται στο σχετικό parameter.

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `token` | `string` | **Required**. Authentication token |
| `packagenumber` | `integer` | Optional. The pagination offset |
| `packagesize` | `integer` | Optional. The actual size of every package (page) |
| `itemIDs` | `string[]` | Required. Array of ITEM_IDs or ITEM_CODEs |




#### Response

```json
{
	"Success": true,
	"Data": [{
	"ITEMIMAGE": "/9j/4AAQSkZJRgAB....AFFFFABRRRQB/9k=", //base64 encoded image blob
	"IMAGETYPE": "Μεσαία", // optional, image size descr
	"IMAGESIZE": "800 x 600", //optional, image size dimens
	"HECODE": "1234", // μοναδικό αναγνωριστικό ή προαιρετικός κωδικός προϊόντος στο ERP (κατοπιν συνεννόησης.)
	"DESCRIPTION": "1234.jpg" // optional, image filename
    }]
}
```


<br><br>

#
## Get product variations

```http
   GET /api/variables
   ```

Επιστρέφει τις διαθέσιμες παραλλαγές όλων των προϊόντων.

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `token` | `string` | **Required**. Authentication token |
| `packagenumber` | `integer` | Optional. The pagination offset |
| `packagesize` | `integer` | Optional. The actual size of every package (page) |



#### Response

```json
{
	"Success": true,
	"Data": [{
	"ITEM_ID": "fdd9529a-8186-ec11-a6c1-d4ae5269035f", //μοναδικό αναγνωριστικό στο ERP (γονικό)
	"ITEM_CODE": "1234", // προαιρετικός κωδικός στο ERP (γονικό)
	"ITEM_BARCODE": "1234567890", // barcode της συγκεκριμένης παραλλαγής
	"ITEMGR_NAME": "Ονομασία προϊόντος - Κόκκινο - L", 
	"I1_NAME": "Κόκκινο", // τιμή χαρακτηριστικού που περιγράφεται από το ακόλουθο id
	"I1_ATTR_ID": "a6ae9500-2f9f-41fa-b7df-bdc71eb79579", // atribute id (στο παράδειγμα, χαρακτηριστικό "ΧΡΩΜΑ")
	"I2_NAME": "L",
	"I2_ATTR_ID": "e5ae19dd-9c3a-4140-8225-c63c28468a71",
    }]
}
```


<br><br>

#
## Get orders status

```http
  GET /api/orders
```

Επιστρέφει όλα τα status παραγγελιών, από την παρεχόμενη ημερομηνία και έπειτα.

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `token` | `string` | **Required**. Authentication token |
| `packagenumber` | `integer` | Optional. The pagination offset |
| `packagesize` | `integer` | Optional. The actual size of every package (page) |
| `dateFrom` | `date` | Required on the "orderDate" call, YYYY-MM-DD |



#### Response

```json
{
	"Success": true,
	"Data": [{
	"ORDER_ID": "fdd9529a-8186-ec11-a6c1-d4ae5269035f", //μοναδικό αναγνωριστικό της παραγγελίας στο ERP 
	"DOCCODE": "1234", // αριθμός της παραγγελίας από το e-shop
	"STATUS": "d400b158-ae74-4ae9-8357-8ae97c4aa632", // μοναδικό αναγνωριστικό της κατάστασης παραγγελίας (πχ "ολοκληρώθηκε")
	"UPDATE_DATE": "2023-09-01"
    }]
}
```


<br><br>

#
## Post new order

```http
  POST /api/order
```

Καταχωρεί στο ERP παραγγελία που προέρχεται από το eshop.

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `token` | `string` | **Required**. Authentication token |
| `OrderDate` | `date` | YYY-MM-DD |
| `CstmTIN` | `string` | Optional. ΑΦΜ πελάτη|
| `CstmName` | `string` | Ονοματεπώνυμο πελάτη |
| `SeriesCode` | `string` | Σειρά παραστατικού |
| `DocNumber` | `string` | Αριθμός παραγγελίας |
| `email` | `string` | E-mail πελάτη |
| `PmmtCode` | `string` | Αναγνωριστικό τρόπου πληρωμής |
| `Payment_City` | `string` | Πόλη χρέωσης |
| `Payment_Prefecture` | `string` | Περιοχή χρέωσης|
| `Payment_Country` | `string` | Χώρα χρέωσης |
| `Payment_Phone1` | `string` | Τηλέφωνο διεύθυνσης χρέωσης |
 `Payment_Phone2` | `string` | Τηλέφωνο 2 διεύθυνσης χρέωσης |
 | `Shipment_Phone1` | `string` | Τηλέφωνο διεύθυνσης αποστολής |
 `Shipment_Phone2` | `string` | Τηλέφωνο 2 διεύθυνσης αποστολής |
| `Shipping` | `string` | Αναγνωριστικό τρόπου αποστολής |
| `Shipment_Address` | `string` | Διεύθυνση αποστολής  |
| `Shipment_Address_num` | `string` | Αριθμός διεύθυνσης αποστολής |
| `Shipment_City` | `string` | Πόλη αποστολής |
| `Shipment_Prefecture` | `string` | Περιοχή αποστολής |
| `Shipment_Country` | `string` | Χώρα αποστολής |
| `Shipment_Zipcode` | `string` | Τ.Κ. αποστολής |
| `Payment_Zipcode` | `string` | Τ.Κ. πληρωμής |
| `order_status_id` | `string` | Αναγνωριστικό κατάστασης παραγγελίας (αρχικό) |
| `Lines` | `Array` | Objects με τα προϊόντα της παραγγελίας |
| `Expenses` | `Array` | Objects με επιπρόσθετες χρεώσεις της παραγγελίας |




#### Request Sample

```json
{
	"OrderDate": "2023-09-01",
	"CstmTIN": "000000000",
	"CstmName": "Demo Customer",
	"SeriesCode": "ΠΑΡ-Λ",
	"DocNumber": "000001",
	"email": "customer@test.gr",
	"PmmtCode": "000001",
	"Payment_City": "THESSALONIKI",
	"Payment_Prefecture": "THESSALONIKI",
	"Payment_Country": "bd3f6a9d-dd83-11e2-be98-00e04c902dfc",
	"Shipping": "f3b2b39d-bcbd-e411-be90-34e6d7230470",
	"Shipment_Address": "Aristotelous",
	"Shipment_Address_num": "20",
	"Shipment_City": "THESSALONIKI",
	"Shipment_Prefecture": "",
	"Shipment_Country": "bd3f6a9d-dd83-11e2-be98-00e04c902dfc",
	"Shipment_Zipcode": "54625",
	"order_status_id": "c611045c-b23f-ec11-a6b9-d4ae52690360",
	"Expenses": [{
		"Title": "6ba5f0ef-a834-11e3-be84-f4b7e2d0efba",
		"Value": 3
	}],
	"Payment_Phone1": "6999999999",
	"TDiscVal": "0",
	"Payment_Phone2": "",
	"Shipment_Phone1": "",
	"Shipment_Phone2": "",
	"Payment_Zipcode": "54645",
	"Lines": [{
		"CenlItemCode": "518",
		"Barcode": "",
		"CenlPrice": 4.7900,
		"CenlAMeasurementQty": 2
	}]
}
```

<br><br>

#